const mongoose = require("mongoose");

const ProductModel = mongoose.model("Product");

//Actions do controller
module.exports = {
  // listagem
  async index(req, res) {

    const { page = 1 } = req.query;
    const produtos = await ProductModel.paginate(
      {}, // filtro que podem ser usadom where por exemplo
      { page: page }, // pagina atual
      { limit: 10 } // Qnt de valores na pagina
    );

    return res.json(produtos);
  },

  // listagem com id
  async show(req, res) {
    const produtos = await ProductModel.findById(req.params.id);

    return res.json(produtos);
  },

  // criação
  async store(req, res) {
    const produtos = await ProductModel.create(req.body);

    return res.json(produtos);
  },

  //update
  async update(req, res) {
    const produtos = await ProductModel.findByIdAndUpdate(
      req.params.id,
      req.body,
      {
        new: true,
      }
    );

    return res.json(produtos);
  },

  //delete
  async destoy(req, res) {
    await ProductModel.findByIdAndRemove(req.params.id);

    return res.json({ ok: { msg: "Removido com sucess" } });
  },
};
