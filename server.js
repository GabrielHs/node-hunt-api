const express = require("express");
const mongoose = require("mongoose");
const requireDir = require("require-dir");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'))

const port = process.env.PORT || 3001;

mongoose.connect(
  "mongodb+srv://gsales:gahepisasa@cluster0-vblza.azure.mongodb.net/test?retryWrites=true&w=majority"
);

requireDir("./src/models");

app.use("/api", require("./src/route"));

app.listen(process.env.PORT || port, () =>
  console.log(
    `app listening on port ${port} with Address http://localhost:${port}/`
  )
);
